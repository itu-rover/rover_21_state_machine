#!/usr/bin/env python

import rospy
import math
import actionlib
from std_msgs.msg import String
from move_base_msgs.msg import MoveBaseActionResult
from move_base_msgs.msg import MoveBaseAction,MoveBaseGoal
from nav_msgs.msg import Odometry
from tf.transformations import quaternion_from_euler
from tf.transformations import euler_from_quaternion
from deneme.msg import Coordinate

class Approach():
    def __init__(self):
            self.approach_goal = MoveBaseGoal()
            self.detection_flag = False
            
            camera_angle = 120
            self.camera_angle_rad = math.pi*camera_angle/180
            self.vehicle_yaw = 0
            self.counter = 0

            rospy.init_node('approach_node',anonymous=False)

            self.client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
            self.client.wait_for_server()

            rospy.Subscriber('/mode',String,self.mode_cb)
            rospy.Subscriber('/odometry/filtered',Odometry,self.odom_cb)
            rospy.Subscriber('/probe/xyz_coordinates',Coordinate,self.coordinate_cb)
            rospy.Subscriber('/probe/detection',String,self.detection_cb)

            self.mode_pub = rospy.Publisher('/mode',String,queue_size = 1)
            
            rospy.spin()

    def odom_cb(self,data):
            vehicle_orientation = [data.pose.pose.orientation.x,data.pose.pose.orientation.y,data.pose.pose.orientation.z,data.pose.pose.orientation.w]
            (vehicle_roll,vehicle_pitch,self.vehicle_yaw) = euler_from_quaternion(vehicle_orientation)
            self.x = data.pose.pose.position.x
            self.y = data.pose.pose.position.y
            self.z = data.pose.pose.position.z
            print(self.vehicle_yaw)

    def detection_cb(self,data):
        if data.data  == 'No Probe':
                self.detection_flag = False
        

    def coordinate_cb(self,data):
            self.approach_goal.target_pose.header.frame_id = 'map'
            self.approach_goal.target_pose.pose.position.x = data.x
            self.approach_goal.target_pose.pose.position.y = data.y
            self.detection_flag = True
            
    def turn_ccw(self):
        self.vehicle_yaw = self.vehicle_yaw - 1.25663706144
        goal = MoveBaseGoal()
        quar = quaternion_from_euler(0,0,self.vehicle_yaw) 
        print(quar[0],quar[1],quar[2],quar[3])
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.orientation.x = quar[0]
        goal.target_pose.pose.orientation.y = quar[1]
        goal.target_pose.pose.orientation.z = quar[2]
        goal.target_pose.pose.orientation.w = quar[3]
        goal.target_pose.pose.position.x = self.x
        goal.target_pose.pose.position.y = self.y
        goal.target_pose.pose.position.z = self.z
        print(rospy.Time.now())
        self.client.send_goal(goal)
        self.wait = self.client.wait_for_result()
        if not self.wait:
            rospy.logerr("Action server not available!")
            rospy.signal_shutdown("Action server not available!")
        else:
            return self.client.get_result()

    def mode_cb(self,data):
        if data.data == "Approach":
            print('Entered Approach Mode')
            while (not self.detection_flag):
                rospy.sleep(5)
                self.turn_ccw()
            self.client.send_goal(self.approach_goal)
            rospy.sleep(2)
            self.mode_pub.publish('Pickup')
            rospy.loginfo('Pickup')
        else:
            return
      
if __name__ == '__main__':
    Approach()