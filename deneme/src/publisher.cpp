#include "ros/ros.h"
#include "deneme/zaman.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle nh;

    ros::Publisher deneme_pub = nh.advertise<deneme::zaman>("msg_time",100);

    ros::Rate loop_rate(10);

    deneme::zaman msg;

    int counter = 0;
    while (ros::ok())
    {
        msg.stamp = ros::Time::now();
        msg.data = counter;

        ROS_INFO("message sent = %d",msg.stamp.sec);

        ROS_INFO("message sent = %d",msg.data);

        deneme_pub.publish(msg);

        loop_rate.sleep();
        
        ++counter;
    }
    

    return 0;
}
