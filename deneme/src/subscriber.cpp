#include "ros/ros.h"
#include "deneme/zaman.h"

void func(const deneme::zaman::ConstPtr& msg){
    ROS_INFO("message received = %d", msg->stamp.sec);
    ROS_INFO("message received = %d", msg->stamp.nsec);
    ROS_INFO("message received = %d", msg->data);

}


int main(int argc, char **argv)
{
    ros::init(argc,argv, "subscriber");
    ros::NodeHandle nh;

    ros::Subscriber deneme_sub = nh.subscribe("msg_time",100,func);

    ros::spin();
    return 0;
}
